import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit {
  user: any = '';
  pelicula: any = '';
  constructor(private active: ActivatedRoute) {}

  ngOnInit(): void {
    // this.active.params.subscribe((data) => {
    //   this.pelicula = data.peli;
    //   fetch('https://reqres.in/api/users/' + data.id)
    //     .then((data) => data.json())
    //     .then((user) => {
    //       this.user = user.data;
    //       console.log(user);
    //     });
    // });
    this.active.queryParams.subscribe((data) => {
        console.log(data);
        this.pelicula = data;
    //   this.pelicula = data.peli;
    //   fetch('https://reqres.in/api/users/' + data.id)
    //     .then((data) => data.json())
    //     .then((user) => {
    //       this.user = user.data;
    //       console.log(user);
    //     });
    });
  }
}
