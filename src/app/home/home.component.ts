import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  peliculas = [
    {
      name: 'Viaje al futuro',
      active: true,
      status: 'success',
      color: '#1ba1e4',
    },
    {
      name: 'Harry Potter y la camara de los secretos',
      active: false,
      status: 'warning',
      color: '#e00dac',
    },
    { name: '50 segundos', active: false, status: 'error', color: '#50bb08' },
    { name: 'contra cara', active: true, status: 'success', color: '#50bb' },
  ];

  jugo: string = '';
  contador: number = 0;
  selected: boolean = false;
  valor: any = [];
  constructor(router: Router) {}

  ngOnInit(): void {
    this.valor = [];
  }

  mostrar(pos) {
    // console.log(pos, this.peliculas[pos]);
    console.log(pos);
  }

  valueToogle(evt){
      console.log(evt);
  }
}
